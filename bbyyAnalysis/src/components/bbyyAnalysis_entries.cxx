#include "../MbbKinFitDecoratorAlg.h"
#include "../BaselineVarsbbyyAlg.h"
#include "../bbyySelectorAlg.h"
#include "../ResonantPNNbbyyAlg.h"
#include "../BoostedVarsbbyyAlg.h"
#include "../bbyyFilterDalitzAlg.h"

DECLARE_COMPONENT(HHBBYY::MbbKinFitDecoratorAlg)
DECLARE_COMPONENT(HHBBYY::BaselineVarsbbyyAlg)
DECLARE_COMPONENT(HHBBYY::bbyySelectorAlg)
DECLARE_COMPONENT(HHBBYY::bbyyFilterDalitzAlg)
DECLARE_COMPONENT(SHBBYY::ResonantPNNbbyyAlg)
DECLARE_COMPONENT(HHBBYY::BoostedVarsbbyyAlg)
