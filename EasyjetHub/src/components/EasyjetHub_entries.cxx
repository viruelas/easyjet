#include "../GN2XTaggingDecoratorAlg.h"
#include "../WTaggingDecoratorAlg.h"
#include "../EventCounterAlg.h"
#include "../JetSelectorAlg.h"
#include "../PhotonSelectorAlg.h"
#include "../MuonSelectorAlg.h"
#include "../ElectronSelectorAlg.h"
#include "../TauSelectorAlg.h"
#include "../EventSelectorAlg.h"
#include "../TruthParentDecoratorAlg.h"
#include "../JetDeepCopyAlg.h"
#include "../TauDecoratorAlg.h"
#include "../JetDecoratorAlg.h"
#include "../TruthParticleInformationAlg.h"
#include "../EventInfoGlobalAlg.h"
#include "../OrthogonalityAlg.h"
#include "../SumOfWeightsAlg.h"
#include "../TruthWBosonInformationAlg.h"
#include "../MetadataHistAlg.h"
#include "../ElectronDecoratorAlg.h"
#include "../LeptonOrderingAlg.h"

using namespace Easyjet;

DECLARE_COMPONENT(GN2XTaggingDecoratorAlg)
DECLARE_COMPONENT(WTaggingDecoratorAlg)
DECLARE_COMPONENT(EventCounterAlg)
DECLARE_COMPONENT(JetSelectorAlg)
DECLARE_COMPONENT(PhotonSelectorAlg)
DECLARE_COMPONENT(MuonSelectorAlg)
DECLARE_COMPONENT(ElectronSelectorAlg)
DECLARE_COMPONENT(TauSelectorAlg)
DECLARE_COMPONENT(EventSelectorAlg)
DECLARE_COMPONENT(TruthParentDecoratorAlg)
DECLARE_COMPONENT(JetDeepCopyAlg)
DECLARE_COMPONENT(TauDecoratorAlg)
DECLARE_COMPONENT(JetDecoratorAlg)
DECLARE_COMPONENT(TruthParticleInformationAlg)
DECLARE_COMPONENT(EventInfoGlobalAlg)
DECLARE_COMPONENT(OrthogonalityAlg)
DECLARE_COMPONENT(SumOfWeightsAlg)
DECLARE_COMPONENT(TruthWBosonInformationAlg)
DECLARE_COMPONENT(MetadataHistAlg)
DECLARE_COMPONENT(ElectronDecoratorAlg)
DECLARE_COMPONENT(LeptonOrderingAlg)
