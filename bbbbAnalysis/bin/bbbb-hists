#!/usr/bin/env python3

from AthenaConfiguration.MainServicesConfig import MainServicesCfg
from AthenaConfiguration.AllConfigFlags import initConfigFlags
from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
from AthenaConfiguration.ComponentFactory import CompFactory

# this should do all the important things for this script
from bbbbAnalysis.config.boost_histograms import histograms_cfg

# main service config misses anything to read files, so you need to
# import both
from AthenaPoolCnvSvc.PoolReadConfig import PoolReadCfg
# set a lower logging level
from AthenaCommon import Constants

from argparse import ArgumentParser
import sys
from pathlib import Path


def parse_args():
    parser = ArgumentParser()
    parser.add_argument('input_files', nargs='+')
    parser.add_argument(
        '-o',
        '--output-hists',
        type=Path,
        default=Path('hists.h5'),
    )
    parser.add_argument('-d','--debug', action='store_true')
    return parser.parse_args()


def core_services_cfg(flags):
    # Get a ComponentAccumulator setting up the standard components
    # needed to run an Athena job.
    cfg = MainServicesCfg(flags)

    # Avoid stack traces to the exception handler. These traces
    # aren't very useful since they just point to the handler, not
    # the original bug.
    cfg.addService(CompFactory.ExceptionSvc(Catch="NONE"))

    cfg.merge(PoolReadCfg(flags))

    # we need this thing for sys-aware algorithms to work
    cfg.addService(CompFactory.CP.SystematicsSvc("SystematicsSvc"))

    return cfg


def run():
    args = parse_args()
    flags = initConfigFlags()
    flags.Input.Files = args.input_files
    if args.debug:
        flags.Exec.DebugStage = 'exec'
    flags.Exec.OutputLevel = Constants.WARNING
    cfg = core_services_cfg(flags)
    cfg.merge(histograms_cfg(
        flags,
        jets="EasyJets",
        out_path=args.output_hists
    ))

    ret = cfg.run()
    sys.exit(0 if ret.isSuccess() else 1)

if __name__ == '__main__':
    run()
